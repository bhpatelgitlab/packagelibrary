using Microsoft.VisualStudio.TestTools.UnitTesting;
using PackageLibrary;
using System;
using System.IO;
using System.Reflection;

namespace PackageLibraryTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        [ExpectedException(typeof(FileNotFoundException))]
        public void FileNotFound()
        {
            string path = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), @"Data\Packages.txt");

            string[] expectedResult = null;
            string[] actualResult = Package.Pack(path);

            Assert.AreEqual(expectedResult, actualResult);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void NoDataInFile()
        {
            string path = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), @"Data\PackageBlank.txt");

            string[] expectedResult = null;
            string[] actualResult = Package.Pack(path);

            Assert.AreEqual(expectedResult, actualResult);
        }

        [TestMethod]
        public void ProcessFileWithDifferentResultSet()
        {
            string path = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), @"Data\Package.txt");
            
            string[] expectedResult = { "Invalid Data", "4", "-", "2,7", "2,3", "Package weight cannot be more than 100" };
            string[] actualResult = Package.Pack(path);

            CollectionAssert.AreEqual(expectedResult, actualResult);
        }
    }
}
