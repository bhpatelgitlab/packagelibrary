Project is a C# class libray and can be used as a reference in application.
Solution also contains PackageLibraryTest project to test the PackageLibrary.
1. Open the sln file in Visual Studio 2019
2. Right click on UnitTest1.cs inside PackageLibraryTest project and select "Run Tests"
3. You will see a popup with all the successful test case execution.
4. You will also find Data folder in that test project, you can change the txt file data as per your test cases.
