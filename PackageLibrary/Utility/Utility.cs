﻿using System;
using System.IO;

namespace PackageLibrary
{
    public static class Utility
    {
        public static string[] ProcessInputFile(string filepath)
        {
            try
            {
                string[] lines = File.ReadAllLines(filepath);
                string[] output = new string[lines.Length];
                int lineCounter = 0;

                if (lines.Length > 0)
                {
                    foreach (var line in lines)
                    {
                        try
                        {
                            int pckWeight;
                            pckWeight = Convert.ToInt32(line.Split(':')[0]);
                            if(pckWeight <= 100)
                            {
                                var packageParams = line.Replace("$", "").Trim().Split(':')[1];
                                var packageItems = packageParams.Trim().Split(' ');

                                int[] itemWeight = new int[packageItems.Length];
                                int[] itemCost = new int[packageItems.Length];
                                int N;
                                int counter = 0;
                                int sum;
                                string result;
                                int[,] dp;

                                foreach (var item in packageItems)
                                {
                                    var _item = item.Replace("(", "").Replace(")", "").Split(',');
                                    itemWeight[counter] = Convert.ToInt32(_item[1]);
                                    itemCost[counter] = Convert.ToInt32(_item[2]);

                                    counter++;
                                }

                                N = itemWeight.Length;
                                dp = new int[N + 1, pckWeight + 1];
                                sum = Service.knapSack(pckWeight, itemWeight, itemCost, N, dp);
                                result = Service.getSelectedItems(N, pckWeight, itemCost, dp);

                                output[lineCounter] = result;
                            }
                            else
                            {
                                output[lineCounter] = Message.PackageWeightConstraint;
                            }
                        }
                        catch
                        {
                            output[lineCounter] = Message.InvalidData;
                        }

                        lineCounter++;
                    }
                }
                else
                {
                    throw new Exception(Message.NoDatainFile);
                }

                return output;
            }
            catch
            {
                throw;
            }
        }
    }
}
