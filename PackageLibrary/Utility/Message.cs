﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PackageLibrary
{
    public class Message
    {
        public static string InvalidData = "Invalid Data";
        public static string PackageWeightConstraint = "Package weight cannot be more than 100";
        public static string NoDatainFile = "There is no data in the input file";
    }
}
