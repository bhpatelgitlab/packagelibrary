﻿using System;
using System.Collections.Generic;

namespace PackageLibrary
{
    public static class Service
    {
        // Method returns the index of selected package item
        public static String getSelectedItems(int n, int W, int[] val, int[,] dp)
        {
            List<int> selectedItems = new List<int>();
            int tempWeight = W;
            int cost = dp[n, tempWeight];
            for (int i = n; i > 0; i--)
            {
                int j = containsCost(cost, i - 1, W, dp);
                if (j == -1)
                {
                    selectedItems.Add(i);
                    cost = cost - val[i - 1];
                }
            }
            selectedItems.Sort();

            return selectedItems.Count > 0 ? String.Join(",", selectedItems) : "-";
        }

        static int containsCost(int n, int i, int W, int[,] dp)
        {
            for (int j = 0; j <= W; j++)
            {
                if (dp[i, j] == n)
                {
                    return j;
                }
            }
            return -1;
        }

        // A utility function that returns
        // maximum of two integers
        static int max(int a, int b)
        {
            return (a > b) ? a : b;
        }

        // Returns the maximum value that
        // can be put in a knapsack of
        // capacity W
        public static int knapSack(int W, int[] wt, int[] val, int n, int[,] K)
        {
            int i, w;

            // Build table K[][] in bottom
            // up manner
            for (i = 0; i <= n; i++)
            {
                for (w = 0; w <= W; w++)
                {
                    if (i == 0 || w == 0)
                        K[i, w] = 0;

                    else if (wt[i - 1] <= w)
                        K[i, w] = Math.Max(
                            val[i - 1]
                            + K[i - 1, w - wt[i - 1]],
                            K[i - 1, w]);
                    else
                        K[i, w] = K[i - 1, w];
                }
            }

            return K[n, W];
        }
    }
}
